/*
 * ate.h
 *
 *  Created on: Nov 21, 2022
 *      Author: issac
 */

#ifndef INC_ATE_H_
#define INC_ATE_H_

/* Includes ----------------------------------------------------------------- */
#include <stdbool.h>
#include "state_machine.h"


/* Exported types ----------------------------------------------------------- */
typedef struct {
	uint16_t vol_adc_12V;
	uint16_t vol_adc_9V;
	uint32_t vol_adc_ratio;
	char datecode[16];
	uint32_t crc32;
} sFACTORY_DATA, *psFACTORY_DATA;


/* Exported constants --------------------------------------------------------*/


/* Public variables --------------------------------------------------------- */


/* Public functions prototypes ---------------------------------------------- */
void ATE_init(pSTATE_MACHINE_TypeDef state);
bool ATE_is_FTEST_pressed(void);
bool ATE_is_valid_factory_data(void);
psFACTORY_DATA ATE_get_factory_data(void);


#endif /* INC_ATE_H_ */
