/*
 * charger.h
 *
 *  Created on: Aug 5, 2022
 *      Author: issac
 */

#ifndef INC_CHARGER_H_
#define INC_CHARGER_H_

/* Includes ----------------------------------------------------------------- */


/* Exported types ----------------------------------------------------------- */
typedef enum {
	CHARGER_ST_DISABLE,
	CHARGER_ST_ENABLE,
} CHARGER_ST;


/* Exported constants --------------------------------------------------------*/


/* Public variables --------------------------------------------------------- */


/* Public functions prototypes ---------------------------------------------- */
void CHARGER_set(CHARGER_ST charger_st);


#endif /* INC_CHARGER_H_ */
