/*
 * motor.h
 *
 *  Created on: Apr 6, 2022
 *      Author: issac
 */

#ifndef INC_MOTOR_H_
#define INC_MOTOR_H_

/* Includes ----------------------------------------------------------------- */


/* Exported types ----------------------------------------------------------- */
typedef enum {
	MOTOR_ST_OFF,
	MOTOR_ST_ON,
} MOTOR_ST;


/* Exported constants --------------------------------------------------------*/


/* Public variables --------------------------------------------------------- */


/* Public functions prototypes ---------------------------------------------- */
void MOTOR_set(MOTOR_ST motor_st);


#endif /* INC_MOTOR_H_ */
