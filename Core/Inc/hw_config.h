/*
 * hw_config.h
 *
 *  Created on: Apr 2, 2022
 *      Author: issac
 */

#ifndef INC_HW_CONFIG_H_
#define INC_HW_CONFIG_H_

/* Includes ----------------------------------------------------------------- */
#include "stm32g0xx_hal.h"


/* Exported types ----------------------------------------------------------- */
/*
 * ADC
 */
#define BAT_ADC_CH			ADC_CHANNEL_0
#define CURRENT_ADC_CH		ADC_CHANNEL_1
#define MOTOR_TEMP_ADC_CH	ADC_CHANNEL_3
//#define BUTTON_2_ADC_CH		ADC_CHANNEL_4
#define TEMP_ADC_CH			ADC_CHANNEL_15


/*
 * USB detect
 */
#define USB_DET_PORT		GPIOA
#define USB_DET_PIN			GPIO_PIN_2
#define USB_DET_CONNECT_ST	GPIO_PIN_SET
#define USB_DET_DISCONN_ST	GPIO_PIN_RESET


/*
 * Buttons
 */
#define BUTTON_1_PORT		GPIOA
#define BUTTON_1_PIN		GPIO_PIN_5
#define BUTTON_1_PRESS_ST	GPIO_PIN_RESET
#define BUTTON_1_RELEASE_ST	GPIO_PIN_SET

#define BUTTON_2_PORT		GPIOA
#define BUTTON_2_PIN		GPIO_PIN_4
#define BUTTON_2_PRESS_ST	GPIO_PIN_RESET
#define BUTTON_2_RELEASE_ST	GPIO_PIN_SET


/*
 * LEDs
 */
#define BLUE_LED_PORT		GPIOA
#define BLUE_LED_PIN		GPIO_PIN_7
#define BLUE_LED_OFF_ST		GPIO_PIN_SET
#define BLUE_LED_ON_ST		GPIO_PIN_RESET

#define RED_LED_PORT		GPIOA
#define RED_LED_PIN			GPIO_PIN_8
#define RED_LED_OFF_ST		GPIO_PIN_SET
#define RED_LED_ON_ST		GPIO_PIN_RESET

#define YELLOW_LED_PORT		GPIOA
#define YELLOW_LED_PIN		GPIO_PIN_13
#define YELLOW_LED_OFF_ST	GPIO_PIN_SET
#define YELLOW_LED_ON_ST	GPIO_PIN_RESET


/*
 * Motor
 */
#define MOTOR_EN_PORT		GPIOA
#define MOTOR_EN_PIN		GPIO_PIN_6
#define MOTOR_EN_ON_ST		GPIO_PIN_SET
#define MOTOR_EN_OFF_ST		GPIO_PIN_RESET


/*
 * Charger
 */
#define CHGR_CTRL_PORT		GPIOA
#define CHGR_CTRL_PIN		GPIO_PIN_12
#define CHGR_CTRL_EN_ST		GPIO_PIN_SET
#define CHGR_CTRL_DIS_ST	GPIO_PIN_RESET


/*
 * FTEST
 */
#define FTEST_PORT			GPIOC
#define FTEST_PIN			GPIO_PIN_14
#define FTEST_PRESS_ST		GPIO_PIN_SET
#define FTEST_RELEASE_ST	GPIO_PIN_RESET


#endif /* INC_HW_CONFIG_H_ */
