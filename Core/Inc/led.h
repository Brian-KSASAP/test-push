/*
 * led.h
 *
 *  Created on: Apr 2, 2022
 *      Author: issac
 */

#ifndef INC_LED_H_
#define INC_LED_H_

/* Includes ----------------------------------------------------------------- */
#include <limits.h>
#include "stm32g0xx_hal.h"


/* Exported types ----------------------------------------------------------- */
typedef enum {
	LED_PIN_ST_OFF,
	LED_PIN_ST_ON,
	/* amount of LED pin state */
	NUM_OF_LED_PIN_ST
} eLED_PIN_ST_T;

typedef struct {
	GPIO_TypeDef *port;
	uint16_t pin;
	GPIO_PinState on_off[NUM_OF_LED_PIN_ST];
} LED_InitTypeDef;

typedef enum {
	LED_ST_UNDEFINED,
	LED_ST_OFF,
	LED_ST_ON,
	LED_ST_FLASH,
	/* amount of LED handle state */
	NUM_OF_LED_ST
} eLED_ST_T;

typedef struct {
	uint32_t cnt;
	uint32_t on_time;
	uint32_t off_time;
} sLED_FlashInitTypeDef;

typedef struct {
	LED_InitTypeDef instant;
	eLED_ST_T curr_state;
	eLED_ST_T next_state;
	uint32_t tickstart;
	sLED_FlashInitTypeDef flash;
} LED_TypeDef, *pLED_TypeDef;


/* Exported constants --------------------------------------------------------*/
#define FLASH_FOREVER		UINT_MAX


/* Public variables --------------------------------------------------------- */
extern LED_TypeDef red_led;
extern LED_TypeDef blue_led;
extern LED_TypeDef yellow_led;

/* Public functions prototypes ---------------------------------------------- */
void LED_init(pLED_TypeDef led_handle, LED_InitTypeDef led_init);
void LED_flash_config(pLED_TypeDef led_handle, sLED_FlashInitTypeDef flash_init);
void LED_set_state(pLED_TypeDef led_handle, eLED_ST_T state);
void LED_process(pLED_TypeDef led_handle);


#endif /* INC_LED_H_ */
