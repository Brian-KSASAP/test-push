/*
 * user_uart.h
 *
 *  Created on: Nov 21, 2022
 *      Author: Systech Issac
 */

#ifndef INC_USER_UART_H_
#define INC_USER_UART_H_

/* Includes ----------------------------------------------------------------- */
#include <stdbool.h>


/* Exported types ----------------------------------------------------------- */


/* Exported constants --------------------------------------------------------*/


/* Public variables --------------------------------------------------------- */


/* Public functions prototypes ---------------------------------------------- */
void UART_init(uint16_t buf_size);
void UART_process(void);
bool UART_is_equal(const char *str);
char *UART_get_line(void);
void UART_write(const char *format, ...);


#endif /* INC_USER_UART_H_ */
