/*
 * state_machine.h
 *
 *  Created on: Apr 3, 2022
 *      Author: issac
 */

#ifndef INC_STATE_MACHINE_H_
#define INC_STATE_MACHINE_H_

/* Includes ----------------------------------------------------------------- */
#include <stdbool.h>
#include "stm32g0xx_hal.h"


/* Exported types ----------------------------------------------------------- */
typedef struct {
	bool implement;
	bool is_overheat;
	bool skip_idle_tickstart_reset;
	void (*curr_state)(void *);
	void (*next_state)(void *);
	uint32_t tickstart;
	uint32_t timeout;
	uint32_t curr_35A_tickstart;
	uint32_t curr_27A_tickstart;
	uint32_t motor_cycle_tickstart;

	/* current ADC value */
	uint16_t battery_8V1_ADC_val;
	uint16_t battery_9V_ADC_val;
	uint16_t battery_10V_ADC_val;
	uint16_t battery_11V1_ADC_val;
	uint16_t battery_11V3_ADC_val;
	uint16_t battery_12V3_ADC_val;
} STATE_MACHINE_TypeDef, *pSTATE_MACHINE_TypeDef;


/* Exported constants --------------------------------------------------------*/


/* Public variables --------------------------------------------------------- */
extern STATE_MACHINE_TypeDef state_machine;


/* Public functions prototypes ---------------------------------------------- */
void STATE_init(pSTATE_MACHINE_TypeDef state);
void STATE_process(pSTATE_MACHINE_TypeDef state_machine);
void STATE_set_calib_data(pSTATE_MACHINE_TypeDef state, uint32_t ratio);


#endif /* INC_STATE_MACHINE_H_ */
