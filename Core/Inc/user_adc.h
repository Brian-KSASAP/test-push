/*
 * user_adc.h
 *
 *  Created on: Apr 5, 2022
 *      Author: issac
 */

#ifndef INC_USER_ADC_H_
#define INC_USER_ADC_H_

/* Includes ----------------------------------------------------------------- */
#include <stdbool.h>
#include "stm32g0xx_hal.h"


/* Exported types ----------------------------------------------------------- */
#define BUF_SIZE	(16)
#define CHANNEL_CNT	(3)

typedef struct {
	uint16_t head;
	uint16_t tail;
	uint16_t cnt;
	uint16_t buf[BUF_SIZE];
} USER_ADC_DataTypeDef, *pUSER_ADC_DataTypeDef;

typedef struct {
	uint16_t avg_val;
	USER_ADC_DataTypeDef data;
} USER_ADC_ChannelTypeDef, *pUSER_ADC_ChannelTypeDef;

typedef struct {
	ADC_HandleTypeDef *hadc;
	uint32_t sampling_interval;
	uint32_t sampling_tick;
	USER_ADC_ChannelTypeDef **channels;
	size_t channels_cnt;
} USER_ADC_TypeDef, *pUSER_ADC_TypeDef;


/* Exported constants --------------------------------------------------------*/
#define BATTERY_8V1_ADC_VAL		2513
#define BATTERY_9V_ADC_VAL		2793
#define BATTERY_10V_ADC_VAL		3341
#define BATTERY_11V1_ADC_VAL	3444
#define BATTERY_11V3_ADC_VAL	3506
//#define BATTERY_12V_ADC_VAL	3724
#define BATTERY_12V3_ADC_VAL	3817

#define CURRENT_27A_ADC_VAL		1889
//#define CURRENT_35A_ADC_VAL		2674

#define TEMP_1C5_ADC_VAL		3554
//#define TEMP_3C_ADC_VAL		2780
//#define TEMP_35C_ADC_VAL		2395
#define TEMP_50C_ADC_VAL		1794
#define TEMP_55C_ADC_VAL		1587
#define TEMP_58C_ADC_VAL		1480
//#define TEMP_60C_ADC_VAL		750
#define TEMP_65C_ADC_VAL		1251

#define BUTTON_PRESSED_ADC_VAL	372


/* Public variables --------------------------------------------------------- */
extern USER_ADC_TypeDef adc_handle;
extern USER_ADC_ChannelTypeDef battery_adc;
extern USER_ADC_ChannelTypeDef current_adc;
extern USER_ADC_ChannelTypeDef motor_temp_adc;
//extern USER_ADC_ChannelTypeDef button2_adc;
extern USER_ADC_ChannelTypeDef batt_temp_adc;


/* Public functions prototypes ---------------------------------------------- */
void ADC_init(pUSER_ADC_TypeDef adc, USER_ADC_TypeDef init);
void ADC_process(pUSER_ADC_TypeDef adc);
bool TEMP_is_higher(uint32_t adc_val, uint32_t cmp_temp);
bool TEMP_is_lower(uint32_t adc_val, uint32_t cmp_temp);
bool BUTTON_is_pressed(uint32_t adc_val);
bool BUTTON_is_released(uint32_t adc_val);


#endif /* INC_USER_ADC_H_ */
