/*
 * button.h
 *
 *  Created on: Apr 2, 2022
 *      Author: issac
 */

#ifndef INC_BUTTON_H_
#define INC_BUTTON_H_


/* Includes ----------------------------------------------------------------- */
#include <stdbool.h>
#include "stm32g0xx_hal.h"


/* Exported types ----------------------------------------------------------- */
typedef enum {
	BUTTON_PIN_ST_RELEASE,
	BUTTON_PIN_ST_PRESSED,
	/* amount of button pin state */
	NUM_OF_BUTTON_PIN_ST
} eButton_PIN_ST_T;

typedef struct {
	GPIO_TypeDef *port;
	uint16_t pin;
	GPIO_PinState pin_st[NUM_OF_BUTTON_PIN_ST];
} Button_InitTypeDef;

typedef enum {
	BUTTON_ST_UNKONWN,
	BUTTON_ST_RELEASED,
	BUTTON_ST_PRESSED,
	/* amount of button handle state */
	NUM_OF_BTTON_ST
} eButton_ST_T;

typedef struct {
	Button_InitTypeDef instant;
	eButton_ST_T state;
	bool is_debouncing;
	bool is_clicked;
	uint32_t debouncing_tickstart;
	uint8_t pressed_cnt;
	uint32_t pressed_tick[2];
} Button_TypeDef, *pButton_TypeDef;


/* Exported constants --------------------------------------------------------*/


/* Public variables --------------------------------------------------------- */
extern Button_TypeDef button_1;
extern Button_TypeDef button_2;
extern Button_TypeDef usb_det;


/* Public functions prototypes ---------------------------------------------- */
void BUTTON_init(pButton_TypeDef button_handle, Button_InitTypeDef btn_init);
void BUTTON_process(pButton_TypeDef button_handle);
bool BUTTON_is_click(pButton_TypeDef hbutton);
void BUTTON_clear_is_click(pButton_TypeDef hbutton);
bool BUTTON_is_double_pressed(pButton_TypeDef hbutton, uint32_t interval);
void BUTTON_clear_timestamp(pButton_TypeDef hbutton);
bool USB_connected(void);


#endif /* INC_BUTTON_H_ */
