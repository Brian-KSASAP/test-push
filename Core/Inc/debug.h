/*
 * debug.h
 *
 *  Created on: Jun 4, 2022
 *      Author: issac
 */

#ifndef INC_DEBUG_H_
#define INC_DEBUG_H_

#define TRACE_LEVEL_OFF			0
#define TRACE_LEVEL_FATAL		1
#define TRACE_LEVEL_ERROR		2
#define TRACE_LEVEL_WARNING		3
#define TRACE_LEVEL_INFO		4
#define TRACE_LEVEL_DEBUG		5

#ifndef TRACE_PRINTF
#define TRACE_PRINTF(...)		Debug_printf(__VA_ARGS__)
#endif

#ifndef TRACE_LEVEL
#define TRACE_LEVEL		TRACE_LEVEL_OFF
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_FATAL)
#define TRACE_FATAL(...)	TRACE_PRINTF("[FATAL] " __VA_ARGS__)
#else
#define TRACE_FATAL(...)
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_ERROR)
#define TRACE_ERROR(...)	TRACE_PRINTF("[ERROR] " __VA_ARGS__)
#else
#define TRACE_ERROR(...)
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_WARNING)
#define TRACE_WARNING(...)	TRACE_PRINTF("[WARN ] " __VA_ARGS__)
#else
#define TRACE_WARNING(...)
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_INFO)
#define TRACE_INFO(...)	TRACE_PRINTF("[INFO ] " __VA_ARGS__)
#else
#define TRACE_INFO(...)
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_DEBUG)
#define TRACE_DEBUG(...)	TRACE_PRINTF("[DEBUG] " __VA_ARGS__)
#else
#define TRACE_DEBUG(...)
#endif

void Debug_printf(const char *format, ...);

#endif /* INC_DEBUG_H_ */
