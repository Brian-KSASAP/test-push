/*
 * user_adc.c
 *
 *  Created on: Apr 5, 2022
 *      Author: issac
 */

/* Includes ----------------------------------------------------------------- */
#include <stdlib.h>
#include <string.h>
#include "user_adc.h"


/* Private typedef ---------------------------------------------------------- */


/* Private variables -------------------------------------------------------- */


/* Public variables --------------------------------------------------------- */
USER_ADC_TypeDef adc_handle = {0};
USER_ADC_ChannelTypeDef battery_adc = {0};
USER_ADC_ChannelTypeDef current_adc = {0};
USER_ADC_ChannelTypeDef motor_temp_adc = {0};
//USER_ADC_ChannelTypeDef button2_adc = {0};
USER_ADC_ChannelTypeDef batt_temp_adc = {0};


/* Private function prototypes ---------------------------------------------- */


/* Private function --------------------------------------------------------- */
void ADC_insert(pUSER_ADC_DataTypeDef data_ptr, uint16_t value)
{
	if (data_ptr->cnt < BUF_SIZE)
	{
		data_ptr->cnt++;
		data_ptr->buf[data_ptr->tail] = value;
		data_ptr->tail = (data_ptr->tail + 1) % BUF_SIZE;
	}
	else
	{
		data_ptr->buf[data_ptr->head] = value;
		data_ptr->head = (data_ptr->head + 1) % BUF_SIZE;
		data_ptr->tail = (data_ptr->tail + 1) % BUF_SIZE;
	}
}

uint16_t ADC_average(pUSER_ADC_DataTypeDef data_ptr)
{
	uint32_t total = 0;
	uint16_t idx;

	idx = data_ptr->head;
	for (uint16_t i = 0; i < data_ptr->cnt; i++)
	{
		total += data_ptr->buf[idx];
		idx = (idx + 1) % BUF_SIZE;
	}

	return (total / data_ptr->cnt);
}

void ADC_read_channel(ADC_HandleTypeDef *hadc, USER_ADC_ChannelTypeDef **channels, size_t size)
{
	HAL_StatusTypeDef ret;
	uint16_t val = 0;
	USER_ADC_ChannelTypeDef *ch = NULL;

	for (uint8_t i = 0; i < size; i++)
	{
		ch = channels[i];

		HAL_ADC_Start(hadc);

		/* wait for ADC conversion */
		ret = HAL_ADC_PollForConversion(hadc, 1000);
		if (ret != HAL_OK)
			continue;

		val = (uint16_t) HAL_ADC_GetValue(hadc);

		ADC_insert(&(ch->data), val);
		ch->avg_val = ADC_average(&(ch->data));
	}
}


/* Public function ---------------------------------------------------------- */
void ADC_init(pUSER_ADC_TypeDef adc, USER_ADC_TypeDef init)
{
	adc->hadc = init.hadc;
	adc->sampling_interval = init.sampling_interval;

	adc->channels = malloc(init.channels_cnt * sizeof(USER_ADC_ChannelTypeDef **));
	if (adc->channels != NULL)
	{
		memcpy(adc->channels, init.channels,
				init.channels_cnt * sizeof(USER_ADC_ChannelTypeDef **));
		adc->channels_cnt = init.channels_cnt;
	}
}

void ADC_process(pUSER_ADC_TypeDef adc)
{
	uint32_t curr;

	curr = HAL_GetTick();
	if (curr < adc->sampling_tick)
		return;

	/* set next sampling time */
	adc->sampling_tick = curr + adc->sampling_interval;

	ADC_read_channel(adc->hadc, adc->channels, adc->channels_cnt);
}

bool TEMP_is_higher(uint32_t adc_val, uint32_t cmp_temp)
{
	if (adc_val <= cmp_temp)
		return true;
	else
		return false;
}

bool TEMP_is_lower(uint32_t adc_val, uint32_t cmp_temp)
{
	if (adc_val >= cmp_temp)
		return true;
	else
		return false;
}

bool BUTTON_is_pressed(uint32_t adc_val)
{
	if (adc_val <= BUTTON_PRESSED_ADC_VAL)
		return true;
	else
		return false;
}

bool BUTTON_is_released(uint32_t adc_val)
{
	if (adc_val > BUTTON_PRESSED_ADC_VAL)
		return true;
	else
		return false;
}
