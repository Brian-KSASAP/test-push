/*
 * user_uart.c
 *
 *  Created on: Nov 21, 2022
 *      Author: Systech Issac
 */

/* Includes ----------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "user_uart.h"
#include "stm32g0xx_hal.h"


/* Private typedef ---------------------------------------------------------- */
typedef struct _CIRCULAR_BUF {
	char *data;
	char *line;
	uint16_t buf_size;
	uint16_t rdI;
	uint16_t wrI;
	uint16_t head;
	uint16_t pendingBytes;
	bool overflow;
	bool is_get_line;
} CIRCULAR_BUF, *psCircularBuf;


/* Private variables -------------------------------------------------------- */
extern UART_HandleTypeDef huart1;

static CIRCULAR_BUF uart_rx = {0};
static uint8_t rx_register;


/* Private function prototypes ---------------------------------------------- */


/* Private function --------------------------------------------------------- */
static void UART_ll_get_line(uint16_t start_idx, uint16_t end_idx)
{
	uint16_t tail_size;

	if (start_idx == end_idx)
		return;

	memset(uart_rx.line, 0x00, uart_rx.buf_size);
	if (start_idx < end_idx)
	{
		memcpy(uart_rx.line, &uart_rx.data[start_idx], end_idx - start_idx);
	}
	else
	{
		tail_size = uart_rx.buf_size - start_idx;
		memcpy(uart_rx.line, &uart_rx.data[start_idx], tail_size);

		if (end_idx != 0)
			memcpy(&uart_rx.line[tail_size], &uart_rx.data[0], end_idx - 1);
	}

	return;
}


/* Public function ---------------------------------------------------------- */
void UART_init(uint16_t buf_size)
{
	uart_rx.buf_size = buf_size;
	uart_rx.data = malloc(buf_size);
	uart_rx.line = malloc(buf_size);

	HAL_UART_Receive_IT(&huart1, &rx_register, 1);
}

void UART_process(void)
{
	bool found = false;

	/* clear line */
	if (uart_rx.is_get_line)
	{
		uart_rx.is_get_line = false;
		memset(uart_rx.line, 0x00, uart_rx.buf_size);
	}

	if (uart_rx.pendingBytes == 0)
		/* do nothing */
		return;

	do {
		if (uart_rx.data[uart_rx.rdI] == '\n')
			found = true;

		/* update index */
		uart_rx.pendingBytes--;
		uart_rx.rdI = (uart_rx.rdI + 1) % uart_rx.buf_size;

		if (found)
		{
			uart_rx.is_get_line = true;
			UART_ll_get_line(uart_rx.head, uart_rx.rdI);
			uart_rx.head = uart_rx.rdI;
			break;
		}

	} while (uart_rx.pendingBytes != 0);
}

bool UART_is_equal(const char *str)
{
	if (!uart_rx.is_get_line)
		return false;

	if (strcmp(uart_rx.line, str) == 0)
		return true;
	else
		return false;
}

char *UART_get_line(void)
{
	if (!uart_rx.is_get_line)
		return NULL;

	return uart_rx.line;
}

void UART_write(const char *format, ...)
{
	char buf[256];
	va_list args;

	va_start(args, format);
	vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);

	HAL_UART_Transmit(&huart1, (uint8_t *)buf, strlen(buf), 500);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	uart_rx.data[uart_rx.wrI] = rx_register;
	uart_rx.wrI = (uart_rx.wrI + 1) % uart_rx.buf_size;
	uart_rx.pendingBytes++;

	HAL_UART_Receive_IT(huart, &rx_register, 1);
}
