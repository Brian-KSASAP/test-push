/*
 * state_machine.c
 *
 *  Created on: Apr 3, 2022
 *      Author: issac
 */
//#define TRACE_LEVEL TRACE_LEVEL_DEBUG

/* Includes ----------------------------------------------------------------- */
#include "main.h"
#include "state_machine.h"
#include "user_adc.h"
#include "button.h"
#include "led.h"
#include "motor.h"
#include "charger.h"
#include "debug.h"


/* Private typedef ---------------------------------------------------------- */
#define MAX_LOOP_CNT			16
#define CURRENT_35A_TIMEOUT		(500)		// 0.5s
#define CURRENT_27A_TIMEOUT		(3 * 1000)	// 3s

#define IDLE_ST_TIMEOUT			(180 * 1000)	// 180s
#define BOTH_BUTTONS_ST_TIMEOUT	(60 * 1000)		// 60s

#define DBLCLK_LOW_VOL_TIMEOUT	(3000)	// 3s
#define DBLCLK_ST1_ON_TIMEOUT	(25000) // 25s on

/* Private variables -------------------------------------------------------- */


/* Public variables --------------------------------------------------------- */
STATE_MACHINE_TypeDef state_machine = {0};


/* Private function prototypes ---------------------------------------------- */
static void STATE_idle(STATE_MACHINE_TypeDef *handle);
static void STATE_USB_connected(STATE_MACHINE_TypeDef *handle);
static void STATE_USB_disconnected_wait(STATE_MACHINE_TypeDef *handle);
static void STATE_low_voltage(STATE_MACHINE_TypeDef *handle);
static void STATE_overheat(STATE_MACHINE_TypeDef *handle);
static void STATE_button1_pressed(pSTATE_MACHINE_TypeDef handle);
static void STATE_button2_pressed(pSTATE_MACHINE_TypeDef handle);
static void STATE_both_buttons_pressed(pSTATE_MACHINE_TypeDef handle);
//static void STATE_cool_down(pSTATE_MACHINE_TypeDef handle);
static void STATE_overcurrent_cool_down(pSTATE_MACHINE_TypeDef handle);
static void STATE_dblclk_overcurrent_cool_down(pSTATE_MACHINE_TypeDef handle);
static void STATE_double_click(pSTATE_MACHINE_TypeDef handle);
static void STATE_double_click_low_voltage(pSTATE_MACHINE_TypeDef handle);
static void STATE_wait_release(pSTATE_MACHINE_TypeDef handle);
static void STATE_sleep(pSTATE_MACHINE_TypeDef handle);
static void STATE_wakeup(pSTATE_MACHINE_TypeDef handle);


/* Private function --------------------------------------------------------- */
static void STATE_set(STATE_MACHINE_TypeDef *state, void (*next_st)(STATE_MACHINE_TypeDef *))
{
	state->implement = true;
	state->next_state = next_st;
}

static bool STATE_implement(STATE_MACHINE_TypeDef *state)
{
	bool tmp = state->implement;

	if (state->implement)
	{
		state->implement = false;
	}

	return tmp;
}

static void POWER_post_stop_config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Configure the main internal regulator output voltage
	*/
	HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

	/** Initializes the RCC Oscillators according to the specified parameters
	* in the RCC_OscInitTypeDef structure.
	*/
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
	RCC_OscInitStruct.PLL.PLLN = 8;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	*/
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
									|RCC_CLOCKTYPE_PCLK1;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
}

static bool STATE_is_overcurrent(uint16_t curr_adc, uint16_t threshold,
		uint32_t *tickstart, uint32_t timeout)
{
	if (curr_adc < threshold)
	{
		if (*tickstart != UINT_MAX)
		{
			TRACE_DEBUG("reset tickstart (%p)\r\n", tickstart);
			*tickstart = UINT_MAX;
		}

		return false;
	}

	/* current ADC value higher than threshold */
	if (*tickstart == UINT_MAX)
	{
		TRACE_DEBUG("set tickstart (%p)\r\n", tickstart);
		*tickstart = HAL_GetTick();
	}

	if (HAL_GetTick() - *tickstart < timeout)
		return false;

	/* current is higher then threshold and exceed timeout */
	return true;
}

static uint32_t STATE_get_flashing_time(sLED_FlashInitTypeDef flash)
{
	return ((flash.on_time + flash.off_time) * flash.cnt);
}

//static uint32_t STATE_low_voltage_LED_flashing(void)
//{
//	const sLED_FlashInitTypeDef low_voltage_flashing = {
//		.cnt = 10,
//		.on_time = 500,
//		.off_time = 500,
//	};
//
//	LED_flash_config(&red_led, low_voltage_flashing);
//	LED_set_state(&red_led, LED_ST_FLASH);
//
//	LED_set_state(&blue_led, LED_ST_OFF);
//
//	return STATE_get_flashing_time(low_voltage_flashing);
//}

static uint32_t STATE_overcurrent_LED_flashing(void)
{
	const sLED_FlashInitTypeDef overcurrent_flashing = {
		.cnt = 5,
		.on_time = 200,
		.off_time = 400,
	};

	LED_flash_config(&red_led, overcurrent_flashing);
	LED_set_state(&red_led, LED_ST_FLASH);

	LED_flash_config(&blue_led, overcurrent_flashing);
	LED_set_state(&blue_led, LED_ST_FLASH);

	/* return blinking time required */
	return STATE_get_flashing_time(overcurrent_flashing);
}

static uint32_t STATE_dblclk_overcurrent_LED_flashing(void)
{
	const sLED_FlashInitTypeDef overcurrent_flashing = {
		.cnt = 17,
		.on_time = 200,
		.off_time = 400,
	};

	LED_flash_config(&red_led, overcurrent_flashing);
	LED_set_state(&red_led, LED_ST_FLASH);

	LED_flash_config(&blue_led, overcurrent_flashing);
	LED_set_state(&blue_led, LED_ST_FLASH);

	/* return blinking time required */
	return STATE_get_flashing_time(overcurrent_flashing);
}

static uint32_t STATE_overheat_LED_flashing(void)
{
	const sLED_FlashInitTypeDef overheat_flash = {
		.cnt = 10,
		.on_time = 500,
		.off_time = 500,
	};

	LED_flash_config(&red_led, overheat_flash);
	LED_set_state(&red_led, LED_ST_FLASH);

	/* return blinking time required */
	return STATE_get_flashing_time(overheat_flash);
}

//static uint32_t STATE_cooldown_LED_flashing(void)
//{
//	const sLED_FlashInitTypeDef cooldown_flash = {
//		.cnt = 6,
//		.on_time = 250,
//		.off_time = 250,
//	};
//
//	LED_flash_config(&red_led, cooldown_flash);
//	LED_set_state(&red_led, LED_ST_FLASH);
//
//	LED_flash_config(&blue_led, cooldown_flash);
//	LED_set_state(&blue_led, LED_ST_FLASH);
//
//	/* return blinking time required */
//	return STATE_get_flashing_time(cooldown_flash);
//}

static void STATE_LED_all_off(void)
{
	LED_set_state(&red_led, LED_ST_OFF);
	LED_set_state(&blue_led, LED_ST_OFF);
	LED_set_state(&yellow_led, LED_ST_OFF);
}

/*
 * state
 */
static void STATE_idle(STATE_MACHINE_TypeDef *handle)
{
	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		if (handle->skip_idle_tickstart_reset)
		{
			handle->skip_idle_tickstart_reset = false;
			return;
		}

		handle->tickstart = HAL_GetTick();
		return;
	}

	/* USB connected action */
	if (USB_connected())
	{
		STATE_set(handle, STATE_USB_connected);
	}
	/* low voltage */
	else if (battery_adc.avg_val < handle->battery_8V1_ADC_val)
	{
		STATE_set(handle, STATE_low_voltage);
	}
	/* over temperature action */
	else if (handle->is_overheat ||
			TEMP_is_higher(batt_temp_adc.avg_val, TEMP_65C_ADC_VAL) ||
			TEMP_is_lower(batt_temp_adc.avg_val, TEMP_1C5_ADC_VAL))
	{
		STATE_set(handle, STATE_overheat);
	}
	/* button pressed action */
	else if (button_1.state == BUTTON_ST_PRESSED &&
//			BUTTON_is_released(button2_adc.avg_val))
			button_2.state == BUTTON_ST_RELEASED)
	{
		STATE_set(handle, STATE_button1_pressed);
	}
	else if (button_1.state == BUTTON_ST_RELEASED &&
//			BUTTON_is_pressed(button2_adc.avg_val))
			button_2.state == BUTTON_ST_PRESSED)
	{
		STATE_set(handle, STATE_button2_pressed);
	}
	else if (button_1.state == BUTTON_ST_PRESSED &&
//			BUTTON_is_pressed(button2_adc.avg_val))
			button_2.state == BUTTON_ST_PRESSED)
	{
		if (BUTTON_is_double_pressed(&button_1, 550))
		{
			BUTTON_clear_timestamp(&button_1);
			STATE_set(handle, STATE_double_click);
		}
		else
		{
			STATE_set(handle, STATE_both_buttons_pressed);
		}
	}
	/* timeout action (sleep) */
	else if (HAL_GetTick() - handle->tickstart >= IDLE_ST_TIMEOUT)
	{
		STATE_set(handle, STATE_sleep);
	}
}

static void STATE_USB_connected(STATE_MACHINE_TypeDef *handle)
{
	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		/* flash blue LED until charging finish */
		LED_flash_config(&blue_led, (sLED_FlashInitTypeDef) {
			.cnt = FLASH_FOREVER,
			.on_time = 1000,
			.off_time = 1000,
		});
		LED_set_state(&blue_led, LED_ST_FLASH);
		return;
	}

	if (battery_adc.avg_val >= handle->battery_12V3_ADC_val)
	{
		LED_set_state(&blue_led, LED_ST_ON);
	}

	if (!USB_connected())
	{
		STATE_set(handle, STATE_USB_disconnected_wait);
	}
	/* over temperature action */
//	else if (TEMP_is_higher(batt_temp_adc.avg_val, TEMP_60C_ADC_VAL) ||
//			TEMP_is_lower(batt_temp_adc.avg_val, TEMP_3C_ADC_VAL))
//	{
//		STATE_LED_all_off();
//		STATE_set(handle, STATE_overheat);
//	}
}

static void STATE_USB_disconnected_wait(STATE_MACHINE_TypeDef *handle)
{
	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		handle->tickstart = HAL_GetTick();
		return;
	}

	/* USB connected again */
	if (USB_connected())
	{
		STATE_set(handle, STATE_USB_connected);
	}
	/* timeout */
	else if (HAL_GetTick() - handle->tickstart >= 3000)
	{
		STATE_LED_all_off();
		STATE_set(handle, STATE_idle);
	}
}

static void STATE_low_voltage(STATE_MACHINE_TypeDef *handle)
{
	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);
		return;
	}

	/* do nothing, wait USB to be connected */
	if (USB_connected())
	{
		STATE_set(handle, STATE_idle);
	}
}

static void STATE_overheat(STATE_MACHINE_TypeDef *handle)
{
	static bool button_check = false;

	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		handle->is_overheat = true;
		CHARGER_set(CHARGER_ST_DISABLE);

		button_check = false;
		handle->timeout = STATE_overheat_LED_flashing();
		handle->tickstart = HAL_GetTick();

		return;
	}

	/* button check */
	if (button_1.state == BUTTON_ST_RELEASED)
		button_check = true;

	if (button_check && button_1.state == BUTTON_ST_PRESSED &&
			HAL_GetTick() >= handle->tickstart + handle->timeout)
	{
		/* release first to enable next button check */
		button_check = false;

		handle->timeout = STATE_overheat_LED_flashing();
		handle->tickstart = HAL_GetTick();
	}

	/* USB check */
	if (USB_connected())
		goto exit;

	/* temperature check */
	if (!TEMP_is_higher(batt_temp_adc.avg_val, TEMP_50C_ADC_VAL) &&
			!TEMP_is_lower(batt_temp_adc.avg_val, TEMP_1C5_ADC_VAL))
	{
		handle->is_overheat = false;
		goto exit;
	}

	return;

exit:
	CHARGER_set(CHARGER_ST_ENABLE);
	LED_set_state(&red_led, LED_ST_OFF);
	STATE_set(handle, STATE_idle);
	return;
}

static void STATE_button1_pressed(pSTATE_MACHINE_TypeDef handle)
{
	void (*next_st)(STATE_MACHINE_TypeDef *) = NULL;

	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		return;
	}

	/* voltage check */
	if (battery_adc.avg_val >= handle->battery_11V3_ADC_val)
	{
		/* turn on blue LED */
		LED_set_state(&red_led, LED_ST_OFF);
		LED_set_state(&blue_led, LED_ST_ON);
	}
	else
	{
		/* turn on red LED */
		LED_set_state(&red_led, LED_ST_ON);
		LED_set_state(&blue_led, LED_ST_OFF);
	}

	/* USB check */
	if (USB_connected())
	{
		next_st = STATE_idle;
		goto exit;
	}

	/* button check */
	if (!(button_1.state == BUTTON_ST_PRESSED &&
//			BUTTON_is_released(button2_adc.avg_val)))
			button_2.state == BUTTON_ST_RELEASED))
	{
		next_st = STATE_idle;
		goto exit;
	}

	/* temperature check */
	if (TEMP_is_higher(batt_temp_adc.avg_val, TEMP_65C_ADC_VAL) ||
			TEMP_is_lower(batt_temp_adc.avg_val, TEMP_1C5_ADC_VAL))
	{
		next_st = STATE_overheat;
		goto exit;
	}

	return;

exit:
	STATE_LED_all_off();
	if (next_st == NULL)
		next_st = STATE_idle;

	STATE_set(handle, next_st);
	return;
}

static void STATE_button2_pressed(pSTATE_MACHINE_TypeDef handle)
{
	void (*next_st)(STATE_MACHINE_TypeDef *) = NULL;

	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		LED_set_state(&yellow_led, LED_ST_ON);
		return;
	}

	/* USB check */
	if (USB_connected())
	{
		next_st = STATE_idle;
		goto exit;
	}

	/* button check */
	if (!(button_1.state == BUTTON_ST_RELEASED &&
//			BUTTON_is_pressed(button2_adc.avg_val)))
			button_2.state == BUTTON_ST_PRESSED))
	{
		/* do not reset idle tickstart within idle and button 2 pressed state */
		handle->skip_idle_tickstart_reset = true;

		next_st = STATE_idle;
		goto exit;
	}

	if (0) {}
	/* temperature check */
	else if (TEMP_is_higher(batt_temp_adc.avg_val, TEMP_65C_ADC_VAL) ||
			TEMP_is_lower(batt_temp_adc.avg_val, TEMP_1C5_ADC_VAL))
	{
		next_st = STATE_overheat;
		goto exit;
	}
	/* idle timeout check */
	else if (HAL_GetTick() - handle->tickstart >= IDLE_ST_TIMEOUT)
	{
		next_st = STATE_sleep;
		goto exit;
	}

	return;

exit:
	LED_set_state(&yellow_led, LED_ST_OFF);

	if (next_st == NULL)
		next_st = STATE_idle;

	STATE_set(handle, next_st);
	return;
}

static void STATE_both_buttons_pressed(pSTATE_MACHINE_TypeDef handle)
{
	void (*next_st)(STATE_MACHINE_TypeDef *) = NULL;
	MOTOR_ST motor_st = MOTOR_ST_ON;

	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		/* reset tickstart */
		handle->curr_27A_tickstart = UINT_MAX;
		handle->curr_35A_tickstart = UINT_MAX;

		handle->tickstart = HAL_GetTick();
		return;
	}

	/* voltage check */
	if (battery_adc.avg_val >= handle->battery_10V_ADC_val)
	{
		LED_set_state(&red_led, LED_ST_OFF);
		LED_set_state(&blue_led, LED_ST_ON);
		LED_set_state(&yellow_led, LED_ST_ON);
	}
	else if (battery_adc.avg_val >= handle->battery_9V_ADC_val)
	{
		LED_set_state(&red_led, LED_ST_OFF);
		LED_set_state(&blue_led, LED_ST_ON);
		LED_set_state(&yellow_led, LED_ST_ON);
	}
	else
	{
		TRACE_WARNING("voltage too low (%d)\r\n", battery_adc.avg_val);

		LED_set_state(&red_led, LED_ST_ON);
		LED_set_state(&blue_led, LED_ST_OFF);
		LED_set_state(&yellow_led, LED_ST_OFF);

		motor_st = MOTOR_ST_OFF;

		STATE_set(handle, STATE_wait_release);
		goto exit;
	}

	/* current check */
	TRACE_DEBUG("V: %4d, C: %4d, T: %4d, MT: %4d\r\n", battery_adc.avg_val,
			current_adc.avg_val, batt_temp_adc.avg_val, motor_temp_adc.avg_val);
#if 0
	if (STATE_is_overcurrent(current_adc.avg_val, CURRENT_35A_ADC_VAL,
			&(handle->curr_35A_tickstart), CURRENT_35A_TIMEOUT))
	{
		TRACE_INFO("current over 35A\r\n");

		/* set LED state */
		STATE_overcurrent_LED_flashing();

		motor_st = MOTOR_ST_OFF;

		STATE_set(handle, STATE_wait_release);
		goto exit;
	}
#endif
	if (STATE_is_overcurrent(current_adc.avg_val, CURRENT_27A_ADC_VAL,
			&(handle->curr_27A_tickstart), CURRENT_27A_TIMEOUT))
	{
		TRACE_INFO("current over 27A\r\n");

		next_st = STATE_overcurrent_cool_down;
		goto next_st_exit;
	}

	/* USB check */
	if (USB_connected())
	{
		next_st = STATE_idle;
		goto next_st_exit;
	}

	/* time check */
	if (HAL_GetTick() - handle->tickstart >= BOTH_BUTTONS_ST_TIMEOUT)
	{
		next_st = STATE_wait_release;
		goto next_st_exit;
	}

	/* temperature check */
	if (TEMP_is_higher(batt_temp_adc.avg_val, TEMP_65C_ADC_VAL) ||
			TEMP_is_lower(batt_temp_adc.avg_val, TEMP_1C5_ADC_VAL))
	{
		next_st = STATE_overheat;
		goto next_st_exit;
	}

	/* buttons check */
	if (!(button_1.state == BUTTON_ST_PRESSED &&
//			BUTTON_is_pressed(button2_adc.avg_val)))
			button_2.state == BUTTON_ST_PRESSED))
	{
		next_st = STATE_idle;
		goto next_st_exit;
	}

exit:
	/* motor control */
	MOTOR_set(motor_st);
	return;

next_st_exit:
	STATE_LED_all_off();
	MOTOR_set(MOTOR_ST_OFF);

	if (next_st == NULL)
		next_st = STATE_idle;

	STATE_set(handle, next_st);
	return;
}

//static void STATE_cool_down(pSTATE_MACHINE_TypeDef handle)
//{
//	if (STATE_implement(handle))
//	{
//		TRACE_INFO("%s\r\n", __FUNCTION__);
//
//		handle->timeout = STATE_cooldown_LED_flashing();
//		handle->tickstart = HAL_GetTick();
//		return;
//	}
//
//	/* cool down for 3s */
//	if (HAL_GetTick() - handle->tickstart >= handle->timeout)
//	{
//		STATE_set(handle, STATE_wait_release);
//		return;
//	}
//}

static void STATE_overcurrent_cool_down(pSTATE_MACHINE_TypeDef handle)
{
	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		handle->timeout = STATE_overcurrent_LED_flashing();
		handle->tickstart = HAL_GetTick();
		return;
	}

	/* cool down for 3s */
	if (HAL_GetTick() - handle->tickstart >= handle->timeout)
	{
		STATE_set(handle, STATE_wait_release);
		return;
	}
}

static void STATE_dblclk_overcurrent_cool_down(pSTATE_MACHINE_TypeDef handle)
{
	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		handle->timeout = STATE_dblclk_overcurrent_LED_flashing();
		handle->tickstart = HAL_GetTick();
		return;
	}

	/* cool down for 10s */
	if (HAL_GetTick() >= handle->tickstart + handle->timeout)
	{
		STATE_set(handle, STATE_wait_release);
		return;
	}
}

static void STATE_double_click(pSTATE_MACHINE_TypeDef handle)
{
	uint32_t curr_tick;
	MOTOR_ST motor_st = MOTOR_ST_ON;
	void (*next_st)(STATE_MACHINE_TypeDef *) = NULL;

	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		/* reset tickstart */
		handle->curr_27A_tickstart = UINT_MAX;
		handle->curr_35A_tickstart = UINT_MAX;

		BUTTON_clear_is_click(&button_1);

		handle->tickstart = HAL_GetTick();
		return;
	}

	/* voltage check */
	if (battery_adc.avg_val >= handle->battery_11V1_ADC_val)
	{
		LED_set_state(&red_led, LED_ST_OFF);
		LED_set_state(&blue_led, LED_ST_ON);
	}
	else if (battery_adc.avg_val >= handle->battery_9V_ADC_val)
	{
		LED_set_state(&red_led, LED_ST_ON);
		LED_set_state(&blue_led, LED_ST_OFF);
	}
	else
	{
		TRACE_WARNING("voltage too low (%d)\r\n", battery_adc.avg_val);

		LED_set_state(&red_led, LED_ST_ON);
		LED_set_state(&blue_led, LED_ST_OFF);

		motor_st = MOTOR_ST_OFF;

		STATE_set(handle, STATE_double_click_low_voltage);
		goto exit;
	}

	/* current check */
	TRACE_DEBUG("V: %4d, C: %4d, T: %4d, MT: %4d\r\n", battery_adc.avg_val,
			current_adc.avg_val, batt_temp_adc.avg_val, motor_temp_adc.avg_val);
#if 0
	if (STATE_is_overcurrent(current_adc.avg_val, CURRENT_35A_ADC_VAL,
			&(handle->curr_35A_tickstart), CURRENT_35A_TIMEOUT))
	{
		TRACE_INFO("current over 35A\r\n");

		/* set LED state */
		STATE_overcurrent_LED_flashing();

		motor_st = MOTOR_ST_OFF;

		STATE_set(handle, STATE_wait_release);
		goto exit;
	}
#endif
	if (STATE_is_overcurrent(current_adc.avg_val, CURRENT_27A_ADC_VAL,
			&(handle->curr_27A_tickstart), CURRENT_27A_TIMEOUT))
	{
		TRACE_INFO("current over 27A\r\n");

		next_st = STATE_dblclk_overcurrent_cool_down;
		goto next_st_exit;
	}

	/* USB check */
	if (USB_connected())
	{
		next_st = STATE_idle;
		goto next_st_exit;
	}

	/* time check */
	curr_tick = HAL_GetTick();
	if (curr_tick <= handle->tickstart + DBLCLK_ST1_ON_TIMEOUT)
	{
		motor_st = MOTOR_ST_ON;
	}
	else
	{
		next_st = STATE_wait_release;
		goto next_st_exit;
	}

	/* temperature check */
	if (TEMP_is_higher(batt_temp_adc.avg_val, TEMP_65C_ADC_VAL) ||
			TEMP_is_lower(batt_temp_adc.avg_val, TEMP_1C5_ADC_VAL))
	{
		next_st = STATE_overheat;
		goto next_st_exit;
	}

	/* button check */
//	if (!(BUTTON_is_pressed(button2_adc.avg_val)))
	if (!(button_2.state == BUTTON_ST_PRESSED))
	{
		next_st = STATE_idle;
		goto next_st_exit;
	}
	else if (BUTTON_is_click(&button_1))
	{
		BUTTON_clear_timestamp(&button_1);

		next_st = STATE_wait_release;
		goto next_st_exit;
	}

exit:
	/* motor control */
	MOTOR_set(motor_st);
	return;

next_st_exit:
	STATE_LED_all_off();
	MOTOR_set(MOTOR_ST_OFF);

	if (next_st == NULL)
		next_st = STATE_idle;

	STATE_set(handle, next_st);
	return;
}

static void STATE_double_click_low_voltage(pSTATE_MACHINE_TypeDef handle)
{
	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);

		handle->tickstart = HAL_GetTick();
		handle->timeout = DBLCLK_LOW_VOL_TIMEOUT;
		return;
	}

	if (HAL_GetTick() >= handle->tickstart + handle->timeout)
	{
		STATE_LED_all_off();
		STATE_set(handle, STATE_idle);
	}
}

static void STATE_wait_release(pSTATE_MACHINE_TypeDef handle)
{
	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);
		return;
	}

	if (button_1.state == BUTTON_ST_RELEASED ||
//			BUTTON_is_released(button2_adc.avg_val))
			button_2.state == BUTTON_ST_RELEASED)
	{
		STATE_LED_all_off();
		STATE_set(handle, STATE_idle);
	}
}

static void STATE_sleep(pSTATE_MACHINE_TypeDef handle)
{
	extern ADC_HandleTypeDef hadc1;
	WAKEUP_REASON_T wakeup;

	if (STATE_implement(handle))
	{
		TRACE_INFO("%s\r\n", __FUNCTION__);
		return;
	}

	/*Disable ADC*/
	ADC_Disable(&hadc1);

	HAL_SuspendTick();

	/* disable SysTick interrupt */
	SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;

	MAIN_clear_wakeup_reason();

	do {
		/* start timer to refresh IWDG */
		MAIN_start_IWDG_refresh_alarm();

		/* enter STOP mode */
		HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFE);

		wakeup = MAIN_get_wakeup_reason();
	} while (wakeup == WAKEUP_BY_ALARM);

	/* post-stop configuration */
	POWER_post_stop_config();

	/* enable SysTick interrupt */
	SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;

	HAL_ResumeTick();

	/*Enable ADC*/
	ADC_Enable(&hadc1);

	STATE_set(handle, STATE_wakeup);
}

static void STATE_wakeup(pSTATE_MACHINE_TypeDef handle)
{
	if (STATE_implement(handle))
	{
		handle->tickstart = HAL_GetTick();
		return;
	}

	if (HAL_GetTick() - handle->tickstart >= 100)
	{
		STATE_set(handle, STATE_idle);
	}
}


/* Public function ---------------------------------------------------------- */
void STATE_init(pSTATE_MACHINE_TypeDef state)
{
	state->implement = true;
	state->is_overheat = false;
	state->skip_idle_tickstart_reset = false;
	state->curr_state = state->next_state = STATE_idle;

	state->curr_27A_tickstart = UINT_MAX;
	state->curr_35A_tickstart = UINT_MAX;

	/* set current ADC default value */
	state->battery_8V1_ADC_val = BATTERY_8V1_ADC_VAL;
	state->battery_9V_ADC_val = BATTERY_9V_ADC_VAL;
	state->battery_10V_ADC_val = BATTERY_10V_ADC_VAL;
	state->battery_11V1_ADC_val = BATTERY_11V1_ADC_VAL;
	state->battery_11V3_ADC_val = BATTERY_11V3_ADC_VAL;
	state->battery_12V3_ADC_val = BATTERY_12V3_ADC_VAL;
}

void STATE_process(pSTATE_MACHINE_TypeDef state_machine)
{
	uint8_t cnt = 0;

	do {
		cnt++;

		state_machine->curr_state(state_machine);

		if (state_machine->curr_state == state_machine->next_state)
			break;

		state_machine->curr_state = state_machine->next_state;

	} while (cnt < MAX_LOOP_CNT);
}

void STATE_set_calib_data(pSTATE_MACHINE_TypeDef state, uint32_t ratio)
{
	/* update current ADC value by factory data */
	state->battery_8V1_ADC_val = (ratio * 81) / 1000;
	state->battery_9V_ADC_val = (ratio * 90) / 1000;
	state->battery_10V_ADC_val = (ratio * 100) / 1000;
	state->battery_11V1_ADC_val = (ratio * 111) / 1000;
	state->battery_11V3_ADC_val = (ratio * 113) / 1000;
	state->battery_12V3_ADC_val = (ratio * 123) / 1000;

	TRACE_DEBUG("8.1V ADC: %d\r\n", state->battery_8V1_ADC_val);
	TRACE_DEBUG("9V ADC: %d\r\n", state->battery_9V_ADC_val);
	TRACE_DEBUG("10V ADC: %d\r\n", state->battery_10V_ADC_val);
	TRACE_DEBUG("11.1V ADC: %d\r\n", state->battery_11V1_ADC_val);
	TRACE_DEBUG("11.3V ADC: %d\r\n", state->battery_11V3_ADC_val);
	TRACE_DEBUG("12.3V ADC: %d\r\n", state->battery_12V3_ADC_val);
}
