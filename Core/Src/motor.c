/*
 * motor.c
 *
 *  Created on: Apr 6, 2022
 *      Author: issac
 */

/* Includes ----------------------------------------------------------------- */
#include "motor.h"
#include "hw_config.h"


/* Private typedef ---------------------------------------------------------- */


/* Private variables -------------------------------------------------------- */


/* Public variables --------------------------------------------------------- */


/* Private function prototypes ---------------------------------------------- */


/* Private function --------------------------------------------------------- */


/* Public function ---------------------------------------------------------- */
void MOTOR_set(MOTOR_ST motor_st)
{
	GPIO_PinState pin_st;

	if (motor_st == MOTOR_ST_ON)
		pin_st = MOTOR_EN_ON_ST;
	else
		pin_st = MOTOR_EN_OFF_ST;

	HAL_GPIO_WritePin(MOTOR_EN_PORT, MOTOR_EN_PIN, pin_st);
}

void MOTOR_toggle(void)
{
	HAL_GPIO_TogglePin(MOTOR_EN_PORT, MOTOR_EN_PIN);
}
