/*
 * led.c
 *
 *  Created on: Apr 2, 2022
 *      Author: issac
 */
//#define TRACE_LEVEL TRACE_LEVEL_DEBUG

/* Includes ----------------------------------------------------------------- */
#include "led.h"
#include "debug.h"


/* Private typedef ---------------------------------------------------------- */


/* Private variables -------------------------------------------------------- */


/* Public variables --------------------------------------------------------- */
LED_TypeDef red_led = {0};
LED_TypeDef blue_led = {0};
LED_TypeDef yellow_led = {0};


/* Private function prototypes ---------------------------------------------- */


/* Private function --------------------------------------------------------- */
static void LED_print_state(pLED_TypeDef led_handle, eLED_ST_T new_state)
{
#if (TRACE_LEVEL >= TRACE_LEVEL_DEBUG)
	char *led_name = NULL;
	char *led_state = NULL;

	if (led_handle == &red_led)
		led_name = "red";
	else if (led_handle == &blue_led)
		led_name = "blue";
	else if (led_handle == &yellow_led)
		led_name = "yellow";
	else
		led_name = "unknown";

	if (new_state == LED_ST_OFF)
		led_state = "off";
	else if (new_state == LED_ST_ON)
		led_state = "on";
	else if (new_state == LED_ST_FLASH)
		led_state = "flashing";
	else
		led_state = "unknown";

	TRACE_DEBUG("set %s LED to %s\r\n", led_name, led_state);
#endif
}

static void LED_set(pLED_TypeDef led_handle, eLED_PIN_ST_T pin_st)
{
	GPIO_TypeDef *port;
	uint16_t pin;
	GPIO_PinState st;

	port = led_handle->instant.port;
	pin = led_handle->instant.pin;
	st = led_handle->instant.on_off[pin_st];

	HAL_GPIO_WritePin(port, pin, st);
}

static eLED_ST_T LED_flashing(pLED_TypeDef led_handle)
{
	eLED_ST_T ret;
	eLED_PIN_ST_T st;
	uint32_t curr_tick;
	uint32_t tickstart, on_time_tickend, off_time_tickend;

	curr_tick = HAL_GetTick();
	tickstart = led_handle->tickstart;
	on_time_tickend = tickstart + led_handle->flash.on_time;
	off_time_tickend = on_time_tickend + led_handle->flash.off_time;

	if (curr_tick >= tickstart && curr_tick < on_time_tickend)
	{
		st = LED_PIN_ST_ON;
		ret = LED_ST_FLASH;
		goto exit;
	}
	else if (curr_tick >= on_time_tickend && curr_tick < off_time_tickend)
	{
		st = LED_PIN_ST_OFF;
		ret = LED_ST_FLASH;
		goto exit;
	}

	if (led_handle->flash.cnt > 0 && led_handle->flash.cnt != FLASH_FOREVER)
		led_handle->flash.cnt--;

	if (led_handle->flash.cnt == 0)
	{
		st = LED_PIN_ST_OFF;
		ret = LED_ST_OFF;
	}
	else
	{
		led_handle->tickstart = curr_tick;

		st = LED_PIN_ST_ON;
		ret = LED_ST_FLASH;
	}

exit:
	LED_set(led_handle, st);
	return ret;
}


/* Public function ---------------------------------------------------------- */
void LED_init(pLED_TypeDef led_handle, LED_InitTypeDef led_init)
{
	led_handle->instant = led_init;
}

void LED_flash_config(pLED_TypeDef led_handle, sLED_FlashInitTypeDef flash_init)
{
	led_handle->tickstart = HAL_GetTick();
	led_handle->flash = flash_init;
}

void LED_set_state(pLED_TypeDef led_handle, eLED_ST_T state)
{
	led_handle->next_state = state;

	if (led_handle->curr_state != led_handle->next_state)
		LED_print_state(led_handle, led_handle->next_state);
}

void LED_process(pLED_TypeDef led_handle)
{
	eLED_ST_T curr_st;

	if (led_handle->curr_state == LED_ST_FLASH &&
			led_handle->next_state == LED_ST_FLASH)
		led_handle->next_state = LED_flashing(led_handle);

	if (led_handle->curr_state == led_handle->next_state)
		return;

	led_handle->curr_state = led_handle->next_state;
	curr_st = led_handle->curr_state;

	if (curr_st == LED_ST_OFF)
	{
		LED_set(led_handle, LED_PIN_ST_OFF);
	}
	else if (curr_st == LED_ST_ON)
	{
		LED_set(led_handle, LED_PIN_ST_ON);
	}
}
