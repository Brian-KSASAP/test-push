/*
 * debug.c
 *
 *  Created on: Jun 4, 2022
 *      Author: issac
 */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "stm32g0xx_hal.h"
#include "debug.h"

extern UART_HandleTypeDef huart1;

void Debug_printf(const char *format, ...)
{
	char buf[256];
	va_list args;

	va_start(args, format);
	vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);

	HAL_UART_Transmit(&huart1, (uint8_t *)buf, strlen(buf), 500);
}
