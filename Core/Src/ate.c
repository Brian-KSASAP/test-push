/*
 * ate.c
 *
 *  Created on: Nov 21, 2022
 *      Author: issac
 */
#define TRACE_LEVEL	TRACE_LEVEL_DEBUG

/* Includes ----------------------------------------------------------------- */
#include <stdlib.h>
#include <string.h>
#include "hw_config.h"
#include "ate.h"
#include "user_uart.h"
#include "user_adc.h"
#include "button.h"
#include "led.h"
#include "motor.h"
#include "debug.h"


/* Private typedef ---------------------------------------------------------- */
#define FACTORY_DATA_ADDR	(0x08007800)

#define ARRAY_SIZE(x)	(sizeof(x) / sizeof(x[0]))

typedef struct {
	const char *name;
	bool (*is_valid_range)(uint16_t adc_value);
	uint16_t *stored_ptr;
} sVOL_CAL_TEST, *psVOL_CAL_TEST;

typedef struct {
	char *name;
	bool (*is_pressed)(void);
	bool (*is_released)(void);
} sBUTTON_TEST, *psBUTTON_TEST;

typedef struct {
	uint8_t test_idx;
	psVOL_CAL_TEST vol_cal_ptr;
	psBUTTON_TEST button_test;
	sFACTORY_DATA factory_data;
} ATE_TypeDef, *pATE_TypeDef;

/* Private function prototypes ---------------------------------------------- */
static void ATE_set(STATE_MACHINE_TypeDef *state, void (*next_st)(STATE_MACHINE_TypeDef *));
static bool ATE_implement(STATE_MACHINE_TypeDef *state);
static void ATE_set_LED_state(uint8_t idx, eLED_ST_T led_state);
static bool ATE_is_valid_12V(uint16_t adc_value);
static bool ATE_is_valid_9V(uint16_t adc_value);
static bool ATE_is_valid_NTC(uint16_t adc_value);
static bool ATE_DS1_is_pressed(void);
static bool ATE_DS1_is_released(void);
static bool ATE_DS2_is_pressed(void);
static bool ATE_DS2_is_released(void);
static bool ATE_is_valid_no_load_current(uint16_t adc_value);
static bool ATE_is_valid_load_current(uint16_t adc_value);

static void ATE_idle(STATE_MACHINE_TypeDef *handle);
static void ATE_test_LED(STATE_MACHINE_TypeDef *handle);
static void ATE_test_LED_on(STATE_MACHINE_TypeDef *handle);
static void ATE_test_LED_next(STATE_MACHINE_TypeDef *handle);
static void ATE_test_LED_done(STATE_MACHINE_TypeDef *handle);
static void ATE_voltage_cal(STATE_MACHINE_TypeDef *handle);
static void ATE_voltage_cal_wait(STATE_MACHINE_TypeDef *handle);
static void ATE_voltage_sampling(STATE_MACHINE_TypeDef *handle);
static void ATE_voltage_cal_done(STATE_MACHINE_TypeDef *handle);
static void ATE_test_NTC(STATE_MACHINE_TypeDef *handle);
static void ATE_test_NTC_retry(STATE_MACHINE_TypeDef *handle);
static void ATE_test_NTC_done(STATE_MACHINE_TypeDef *handle);
static void ATE_test_button(STATE_MACHINE_TypeDef *handle);
static void ATE_test_button_released(STATE_MACHINE_TypeDef *handle);
static void ATE_test_button_pressed(STATE_MACHINE_TypeDef *handle);
static void ATE_test_button_done(STATE_MACHINE_TypeDef *handle);
static void ATE_test_motor(STATE_MACHINE_TypeDef *handle);
static void ATE_test_motor_no_load(STATE_MACHINE_TypeDef *handle);
static void ATE_test_motor_wait_pw(STATE_MACHINE_TypeDef *handle);
static void ATE_test_motor_with_load(STATE_MACHINE_TypeDef *handle);
static void ATE_test_motor_done(STATE_MACHINE_TypeDef *handle);
static void ATE_test_datecode(STATE_MACHINE_TypeDef *handle);
static void ATE_test_save(STATE_MACHINE_TypeDef *handle);
static void ATE_test_done(STATE_MACHINE_TypeDef *handle);


/* Private variables -------------------------------------------------------- */
static ATE_TypeDef ate_info = {0};

static sVOL_CAL_TEST vol_cal_list[] = {
		{
			.name = "12V",
			.is_valid_range = ATE_is_valid_12V,
			.stored_ptr = &ate_info.factory_data.vol_adc_12V,
//		}, {
//			.name = "9V",
//			.is_valid_range = ATE_is_valid_9V,
//			.stored_ptr = &ate_info.factory_data.vol_adc_9V,
		}
};

static sBUTTON_TEST button_list[] = {
		{
			.name = "DS1",
			.is_pressed = ATE_DS1_is_pressed,
			.is_released = ATE_DS1_is_released,
		}, {
			.name = "DS2",
			.is_pressed = ATE_DS2_is_pressed,
			.is_released = ATE_DS2_is_released,
		}
};

extern CRC_HandleTypeDef hcrc;

/* Public variables --------------------------------------------------------- */


/* Private function --------------------------------------------------------- */
static void ATE_set(STATE_MACHINE_TypeDef *state, void (*next_st)(STATE_MACHINE_TypeDef *))
{
	state->implement = true;
	state->next_state = next_st;
}

static bool ATE_implement(STATE_MACHINE_TypeDef *state)
{
	bool tmp = state->implement;

	if (state->implement)
	{
		state->implement = false;
	}

	return tmp;
}

static void ATE_set_LED_state(uint8_t idx, eLED_ST_T led_state)
{
	bool err = false;
	char *led_state_str, *led_color;

	switch (led_state)
	{
	case LED_ST_ON:
		led_state_str = "On";
		break;
	case LED_ST_OFF:
		led_state_str = "Off";
		break;
	default:
		led_state_str = "Unknown";
		break;
	}

	switch(idx)
	{
	case 0:
		/* blue LED */
		LED_set_state(&blue_led, led_state);
		led_color = "BLUE";
		break;
	case 1:
		/* red LED */
		LED_set_state(&red_led, led_state);
		led_color = "RED";
		break;
	case 2:
		/* yellow LED */
		LED_set_state(&yellow_led, led_state);
		led_color = "YELLOW";
		break;
	default:
		err = true;
		break;
	}

	if (!err)
		UART_write("%s LED %s\r\n", led_color, led_state_str);
}

static bool ATE_is_valid_12V(uint16_t adc_value)
{
//	if (adc_value >= 3451 && adc_value <= 3723)
	if (adc_value >= 3451 && adc_value <= 4010)
		return true;
	else
		return false;
}

static bool ATE_is_valid_9V(uint16_t adc_value)
{
	if (adc_value >= 2588 && adc_value <= 2792)
		return true;
	else
		return false;
}

static bool ATE_is_valid_NTC(uint16_t adc_value)
{
	if (adc_value >= 2324 && adc_value <= 2930)
		return true;
	else
		return false;
}

static bool ATE_DS1_is_pressed(void)
{
	return (button_1.state == BUTTON_ST_PRESSED);
}

static bool ATE_DS1_is_released(void)
{
	return (button_1.state == BUTTON_ST_RELEASED);
}

static bool ATE_DS2_is_pressed(void)
{
//	return BUTTON_is_pressed(button2_adc.avg_val);
	return (button_2.state == BUTTON_ST_PRESSED);
}

static bool ATE_DS2_is_released(void)
{
//	return BUTTON_is_released(button2_adc.avg_val);
	return (button_2.state == BUTTON_ST_RELEASED);
}

static bool ATE_is_valid_no_load_current(uint16_t adc_value)
{
//	if (adc_value >= 100 && adc_value <= 200)
	if (adc_value >= 40 && adc_value <= 250)
		return true;
	else
		return false;
}

static bool ATE_is_valid_load_current(uint16_t adc_value)
{
	if (adc_value >= 1500 && adc_value <= 2500)
		return true;
	else
		return false;
}

static uint16_t ATE_strtoul(char *str, uint8_t len)
{
	uint16_t ret;
	char tmp[8] = {0};

	strncpy(tmp, str, len);
	ret = (uint16_t) strtoul(tmp, (char **)NULL, 10);

	return ret;
}

static bool ATE_is_valid_datecode(char *datecode)
{
	bool is_leap_year = false;
	bool is_31_days_mon = false;
	bool is_Feb = false;
	char *ptr = datecode;
	uint16_t val, day_max;
	uint8_t month_31_days[] = {1, 3, 5, 7, 8, 10, 12};

	/* check 4 bytes for year */
	val = ATE_strtoul(ptr, 4);
	ptr += 4;

	if (val < 2022)
		return false;

	if ((val % 4) == 0)
	{
		if ((val % 100) != 0)
			is_leap_year = true;
		else if ((val % 400) == 0)
			is_leap_year = true;
	}

	/* check 2 bytes for month */
	val = ATE_strtoul(ptr, 2);
	ptr += 2;

	if (val == 0 || val > 12)
		return false;

	for (uint8_t i = 0; i < ARRAY_SIZE(month_31_days); i++)
	{
		if (val == month_31_days[i])
		{
			is_31_days_mon = true;
			break;
		}
	}

	if (val == 2)
		is_Feb = true;

	/* check 2 bytes for day */
	val = ATE_strtoul(ptr, 2);
	ptr += 2;

	if (is_31_days_mon)
	{
		day_max = 31;
	}
	else if (is_Feb)
	{
		if (is_leap_year)
			day_max = 29;
		else
			day_max = 28;
	}
	else
	{
		day_max = 30;
	}

	if (val == 0 || val > day_max)
		return false;

	/* check "_" */
	if (*ptr != '_')
		return false;

	ptr++;

	/* check 2 bytes for hour */
	val = ATE_strtoul(ptr, 2);
	ptr += 2;

	if (val >= 24)
		return false;

	/* check 2 bytes for minute */
	val = ATE_strtoul(ptr, 2);
	ptr += 2;

	if (val >= 60)
		return false;

	/* check 2 bytes for second */
	val = ATE_strtoul(ptr, 2);
	ptr += 2;

	if (val >= 60)
		return false;

	/* all check */
	return true;
}

static uint32_t ATE_calc_CRC32(void *data, size_t size)
{
	return HAL_CRC_Calculate(&hcrc, data, size);
}

static HAL_StatusTypeDef ATE_flash_program(uint32_t addr, void *data, size_t size)
{
	uint8_t tmp[8] = {0};
	uint8_t written;
	HAL_StatusTypeDef ret;

	while (size > 0)
	{
		if (size >= 8)
		{
			ret = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, addr,
					*(uint64_t *)data);
			if (ret != HAL_OK)
				return ret;

			written = 8;
		}
		else
		{
			memcpy(&tmp, (void *)data, size);
			ret = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, addr,
					*(uint64_t *)tmp);
			if (ret != HAL_OK)
				return ret;

			written = size;
		}

		addr += written;
		data += written;
		size -= written;
	}

	return HAL_OK;
}

/*
 * state
 */
static void ATE_idle(STATE_MACHINE_TypeDef *handle)
{
	if (ATE_implement(handle))
	{
		return;
	}

	if (UART_is_equal("ftest\n"))
	{
		UART_write("FTEST OK!\r\n");
		ATE_set(handle, ATE_test_LED);
	}

	return;
}

static void ATE_test_LED(STATE_MACHINE_TypeDef *handle)
{
	ate_info.test_idx = 0;
	ATE_set(handle, ATE_test_LED_on);

	return;
}

static void ATE_test_LED_on(STATE_MACHINE_TypeDef *handle)
{
	void (*next_st)(STATE_MACHINE_TypeDef *) = NULL;

	if (ATE_implement(handle))
	{
		ATE_set_LED_state(ate_info.test_idx, LED_ST_ON);

		handle->tickstart = HAL_GetTick();
		return;
	}

	if (UART_is_equal("\n"))
	{
		next_st = ATE_test_LED_done;
		goto exit;
	}
	/* timeout, next LED */
	else if (HAL_GetTick() - handle->tickstart >= 1000)
	{
		next_st = ATE_test_LED_next;
		goto exit;
	}

	return;

exit:
	ATE_set_LED_state(ate_info.test_idx, LED_ST_OFF);

	if (next_st != NULL)
		ATE_set(handle, next_st);
	return;
}

static void ATE_test_LED_next(STATE_MACHINE_TypeDef *handle)
{
	ate_info.test_idx = (ate_info.test_idx + 1) % 3;
	ATE_set(handle, ATE_test_LED_on);
	return;
}

static void ATE_test_LED_done(STATE_MACHINE_TypeDef *handle)
{
	UART_write("LED test done!\r\n");

	ATE_set(handle, ATE_voltage_cal);
	return;
}

static void ATE_voltage_cal(STATE_MACHINE_TypeDef *handle)
{
	UART_write("Voltage calibration\r\n");
	ate_info.test_idx = 0;

	ATE_set(handle, ATE_voltage_cal_wait);
	return;
}

static void ATE_voltage_cal_wait(STATE_MACHINE_TypeDef *handle)
{
	if (ATE_implement(handle))
	{
		if (ate_info.test_idx >= ARRAY_SIZE(vol_cal_list))
		{
			/* all done */
			ATE_set(handle, ATE_voltage_cal_done);
			return;
		}

		ate_info.vol_cal_ptr = &vol_cal_list[ate_info.test_idx];

		UART_write("Please set Vbat to %s, press enter to continue\r\n",
				ate_info.vol_cal_ptr->name);

		return;
	}

	if (UART_is_equal("\n"))
	{
		ATE_set(handle, ATE_voltage_sampling);
	}
}

static void ATE_voltage_sampling(STATE_MACHINE_TypeDef *handle)
{
	if (ATE_implement(handle))
	{
		handle->tickstart = HAL_GetTick();
		return;
	}

	if (HAL_GetTick() - handle->tickstart >= 200)
	{
		if (ate_info.vol_cal_ptr->is_valid_range(battery_adc.avg_val))
		{
			/* save ADC value */
			*(ate_info.vol_cal_ptr->stored_ptr) = battery_adc.avg_val;

			/* update index */
			ate_info.test_idx++;

			UART_write("%s OK!\r\n", ate_info.vol_cal_ptr->name);
			goto exit;
		}
		else
		{
			UART_write("ADC value %d out of range\r\n", battery_adc.avg_val);
			goto exit;
		}
	}

	return;

exit:
	ATE_set(handle, ATE_voltage_cal_wait);
	return;
}

static void ATE_voltage_cal_done(STATE_MACHINE_TypeDef *handle)
{
	ate_info.factory_data.vol_adc_ratio = (ate_info.factory_data.vol_adc_12V * 100) / 12;

	UART_write("12V ADC value: %d, ratio: %d.%02d\r\n",
			ate_info.factory_data.vol_adc_12V,
			ate_info.factory_data.vol_adc_ratio / 100,
			ate_info.factory_data.vol_adc_ratio % 100);
//	UART_write("9V ADC value: %d\r\n", ate_info.factory_data.vol_adc_9V);

	ATE_set(handle, ATE_test_NTC);
	return;
}

static void ATE_test_NTC(STATE_MACHINE_TypeDef *handle)
{
	if (ATE_implement(handle))
	{
		UART_write("NTC check!\r\n");

		handle->tickstart = HAL_GetTick();
		return;
	}

	if (HAL_GetTick() - handle->tickstart >= 200)
	{
		if (ATE_is_valid_NTC(batt_temp_adc.avg_val))
		{
			UART_write("NTC OK!\r\n");
			ATE_set(handle, ATE_test_NTC_done);
		}
		else
		{
			UART_write("ADC value %d out of range\r\n", batt_temp_adc.avg_val);
			ATE_set(handle, ATE_test_NTC_retry);
		}
	}
}

static void ATE_test_NTC_retry(STATE_MACHINE_TypeDef *handle)
{
	if (ATE_implement(handle))
	{
		handle->tickstart = HAL_GetTick();
		return;
	}

	if (HAL_GetTick() - handle->tickstart >= 1000)
	{
		ATE_set(handle, ATE_test_NTC);
		return;
	}
}

static void ATE_test_NTC_done(STATE_MACHINE_TypeDef *handle)
{
	UART_write("NTC value: %d\r\n", batt_temp_adc.avg_val);

	ATE_set(handle, ATE_test_button);
	return;
}

static void ATE_test_button(STATE_MACHINE_TypeDef *handle)
{
	UART_write("Button test!\r\n");
	ate_info.test_idx = 0;

	ATE_set(handle, ATE_test_button_released);
	return;
}

static void ATE_test_button_released(STATE_MACHINE_TypeDef *handle)
{
	if (ATE_implement(handle))
	{
		if (ate_info.test_idx >= ARRAY_SIZE(button_list))
		{
			/* all done */
			ATE_set(handle, ATE_test_button_done);
			return;
		}

		ate_info.button_test = &button_list[ate_info.test_idx];
		return;
	}

	if (ate_info.button_test->is_released())
	{
		ATE_set(handle, ATE_test_button_pressed);
		return;
	}
}

static void ATE_test_button_pressed(STATE_MACHINE_TypeDef *handle)
{
	if (ATE_implement(handle))
	{
		UART_write("press %s\r\n", ate_info.button_test->name);
		return;
	}

	if (ate_info.button_test->is_pressed())
	{
		UART_write("%s OK!\r\n", ate_info.button_test->name);
		ate_info.test_idx++;
		ATE_set(handle, ATE_test_button_released);
	}
}

static void ATE_test_button_done(STATE_MACHINE_TypeDef *handle)
{
	UART_write("Buttons OK!\r\n");
	ATE_set(handle, ATE_test_motor);
	return;
}

static void ATE_test_motor(STATE_MACHINE_TypeDef *handle)
{
	UART_write("motor check!\r\n");
	ATE_set(handle, ATE_test_motor_no_load);
	return;
}

static void ATE_test_motor_no_load(STATE_MACHINE_TypeDef *handle)
{
	if (ATE_implement(handle))
	{
		UART_write("no load current check!\r\n");

		handle->tickstart = HAL_GetTick();
		return;
	}

	if (HAL_GetTick() - handle->tickstart >= 500)
	{
		handle->tickstart = HAL_GetTick();

		UART_write("current ADC: %d\r\n", current_adc.avg_val);

		if (ATE_is_valid_no_load_current(current_adc.avg_val))
		{
			UART_write("OK!\r\n");
			ATE_set(handle, ATE_test_motor_wait_pw);
		}
		else
		{
			UART_write("ADC value %d out of range\r\n", current_adc.avg_val);
		}
	}
}

static void ATE_test_motor_wait_pw(STATE_MACHINE_TypeDef *handle)
{
	if (ATE_implement(handle))
	{
		UART_write("Please enter motor on password\r\n");
		UART_write("DANGER OPERATION!\r\n");

		return;
	}

	if (UART_is_equal("motor!\n"))
	{
		ATE_set(handle, ATE_test_motor_with_load);
	}
}

static void ATE_test_motor_with_load(STATE_MACHINE_TypeDef *handle)
{
	void (*next_st)(STATE_MACHINE_TypeDef *) = NULL;
	if (ATE_implement(handle))
	{
		UART_write("Motor ON!\r\n");
		MOTOR_set(MOTOR_ST_ON);

		handle->tickstart = HAL_GetTick();
		handle->curr_27A_tickstart = HAL_GetTick();
		return;
	}

	if (HAL_GetTick() - handle->tickstart >= 500)
	{
		handle->tickstart = HAL_GetTick();
		UART_write("current ADC: %d\r\n", current_adc.avg_val);

		if (ATE_is_valid_load_current(current_adc.avg_val))
		{
			UART_write("OK!");
			next_st = ATE_test_motor_done;
			goto exit;
		}
		else
		{
			UART_write("ADC value %d out of range\r\n", current_adc.avg_val);

			if (HAL_GetTick() - handle->curr_27A_tickstart >= 10000)
			{
				UART_write("timeout, motor OFF!\r\n");

				next_st = ATE_idle;
				goto exit;
			}
		}
	}

	return;

exit:
	MOTOR_set(MOTOR_ST_OFF);
	if (next_st == NULL)
		next_st = ATE_idle;
	ATE_set(handle, next_st);
	return;
}

static void ATE_test_motor_done(STATE_MACHINE_TypeDef *handle)
{
	UART_write("motor test OK!\r\n");
	ATE_set(handle, ATE_test_datecode);
	return;
}

static void ATE_test_datecode(STATE_MACHINE_TypeDef *handle)
{
	char *input, *ptr;

	if (ATE_implement(handle))
	{
		UART_write("All test OK!\r\n");
		UART_write("Please input date code\r\n");

		return;
	}

	input = UART_get_line();
	if (input != NULL)
	{
		if (strlen(input) < 15)
		{
			UART_write("datecode too short\r\n");
			goto fail_exit;
		}

		if (!ATE_is_valid_datecode(input))
		{
			UART_write("datecode invalid\r\n");
			goto fail_exit;
		}

		strncpy(ate_info.factory_data.datecode, input,
				sizeof(ate_info.factory_data.datecode));

		/* remove ending "\r\n" */
		ptr = &ate_info.factory_data.datecode[strlen(ate_info.factory_data.datecode) - 1];
		while (ptr >= &ate_info.factory_data.datecode[0])
		{
			if (*ptr == '\r' || *ptr == '\n')
			{
				*ptr = '\0';
				ptr--;
			}
			else
			{
				break;
			}
		}

		UART_write("datecode: %s\r\n", ate_info.factory_data.datecode);
		UART_write("PCB test complete!\r\n");

		ATE_set(handle, ATE_test_save);
	}

	return;

fail_exit:
	UART_write("wrong format, please re-test\r\n");
	return;
}

static void ATE_test_save(STATE_MACHINE_TypeDef *handle)
{
	HAL_StatusTypeDef ret;
	FLASH_EraseInitTypeDef EraseInitStruct;
	uint32_t error, checksum;

	checksum = ATE_calc_CRC32(&ate_info.factory_data,
			sizeof(ate_info.factory_data) - 4);
	ate_info.factory_data.crc32 = checksum;

	HAL_FLASH_Unlock();

	/* erase */
	EraseInitStruct = (FLASH_EraseInitTypeDef) {
		.TypeErase = FLASH_TYPEERASE_PAGES,
		.Banks = FLASH_BANK_1,
		.Page = 15,
		.NbPages = 1,
	};
	ret = HAL_FLASHEx_Erase(&EraseInitStruct, &error);
	if (ret != HAL_OK)
	{
		UART_write("erase Flash failed\r\n");
		goto fail_exit;
	}

	/* write */
	ret = ATE_flash_program(FACTORY_DATA_ADDR, &ate_info.factory_data,
			sizeof(ate_info.factory_data));
	if (ret != HAL_OK)
	{
		UART_write("write Flash failed\r\n");
		goto fail_exit;
	}

fail_exit:
	HAL_FLASH_Lock();

	ATE_set(handle, ATE_test_done);
	return;
}

static void ATE_test_done(STATE_MACHINE_TypeDef *handle)
{
	/* do nothing */
}

/* Public function ---------------------------------------------------------- */
void ATE_init(pSTATE_MACHINE_TypeDef state)
{
	UART_init(64);

	memset(&ate_info, 0x00, sizeof(ate_info));

	state->implement = true;
	state->is_overheat = false;
	state->curr_state = state->next_state = ATE_idle;
}

bool ATE_is_FTEST_pressed(void)
{
	bool ret = true;
	GPIO_PinState state;

	for (uint8_t i = 0; i < 10; i++)
	{
		state = HAL_GPIO_ReadPin(FTEST_PORT, FTEST_PIN);
		TRACE_DEBUG("FTEST: %d (%d)\r\n", state, i);
		if (HAL_GPIO_ReadPin(FTEST_PORT, FTEST_PIN) == FTEST_RELEASE_ST)
		{
			TRACE_DEBUG("FTEST is not pressed\r\n");
			ret = false;
			break;
		}

		HAL_Delay(5);
	}

	return ret;
}

bool ATE_is_valid_factory_data(void)
{
	uint32_t calc_crc;
	psFACTORY_DATA ptr = (void *)FACTORY_DATA_ADDR;

	calc_crc = ATE_calc_CRC32(ptr, sizeof(sFACTORY_DATA) - 4);
	if (calc_crc == ptr->crc32)
		return true;
	else
		return false;
}

psFACTORY_DATA ATE_get_factory_data(void)
{
	return ((void *)FACTORY_DATA_ADDR);
}
