/*
 * button.c
 *
 *  Created on: Apr 2, 2022
 *      Author: issac
 */

/* Includes ----------------------------------------------------------------- */
#include "hw_config.h"
#include "button.h"


/* Private typedef ---------------------------------------------------------- */


/* Private variables -------------------------------------------------------- */
Button_TypeDef button_1 = {0};
Button_TypeDef button_2 = {0};


/* Public variables --------------------------------------------------------- */


/* Private function prototypes ---------------------------------------------- */


/* Private function --------------------------------------------------------- */
static GPIO_PinState BUTTON_read(pButton_TypeDef hbutton)
{
	GPIO_TypeDef *port;
	uint16_t pin;

	port = hbutton->instant.port;
	pin = hbutton->instant.pin;

	return HAL_GPIO_ReadPin(port, pin);
}

static eButton_ST_T BUTTON_debouncing(pButton_TypeDef hbutton, eButton_ST_T new_st)
{
	eButton_ST_T ret_st = hbutton->state;

	if (hbutton->is_debouncing)
	{
		if (HAL_GetTick() - hbutton->debouncing_tickstart > 50)
		{
			hbutton->is_debouncing = false;
			ret_st = new_st;
		}
	}
	else
	{
		/* start debouncing */
		hbutton->is_debouncing = true;
		hbutton->debouncing_tickstart = HAL_GetTick();
	}

	return ret_st;
}

static void BUTTON_mark_pressed_time(pButton_TypeDef hbutton)
{
	if (hbutton->is_debouncing)
		return;

	if (hbutton->state == BUTTON_ST_PRESSED)
	{
		if (hbutton->pressed_cnt == 0)
		{
			hbutton->pressed_cnt++;
			hbutton->pressed_tick[0] = HAL_GetTick();
		}
		else if (hbutton->pressed_cnt == 1)
		{
			hbutton->pressed_cnt++;
			hbutton->pressed_tick[1] = HAL_GetTick();
		}
		else
		{
			hbutton->pressed_tick[0] = hbutton->pressed_tick[1];
			hbutton->pressed_tick[1] = HAL_GetTick();
		}
	}
}

static bool BUTTON_mark_is_click(eButton_ST_T org, eButton_ST_T new)
{
	return (org == BUTTON_ST_RELEASED && new == BUTTON_ST_PRESSED);
}


/* Public function ---------------------------------------------------------- */
void BUTTON_init(pButton_TypeDef hbutton, Button_InitTypeDef btn_init)
{
	hbutton->instant = btn_init;
}

#if 0
void BUTTON_process(pButton_TypeDef hbutton)
{
	GPIO_PinState pin_st = BUTTON_read(hbutton);
	eButton_ST_T new_st;

	if (pin_st == hbutton->instant.pin_st[BUTTON_PIN_ST_RELEASE])
		new_st = BUTTON_ST_RELEASED;
	else
		new_st = BUTTON_ST_PRESSED;

	if (hbutton->state == new_st)
		return;

	hbutton->state = new_st;
	if (hbutton->state == BUTTON_ST_PRESSED)
	{
		if (hbutton->pressed_cnt == 0)
		{
			hbutton->pressed_tick[0] = HAL_GetTick();
			hbutton->pressed_cnt++;
		}
		else if (hbutton->pressed_cnt == 1)
		{
			hbutton->pressed_tick[1] = HAL_GetTick();
			hbutton->pressed_cnt++;
		}
		else
		{
			hbutton->pressed_tick[0] = hbutton->pressed_tick[1];
			hbutton->pressed_tick[1] = HAL_GetTick();
		}
	}
}
#else

void BUTTON_process(pButton_TypeDef hbutton)
{
	GPIO_PinState pin_st = BUTTON_read(hbutton);
	eButton_ST_T new_st, original_st;

	if (pin_st == hbutton->instant.pin_st[BUTTON_PIN_ST_RELEASE])
		new_st = BUTTON_ST_RELEASED;
	else
		new_st = BUTTON_ST_PRESSED;

	if (hbutton->state == new_st)
	{
		if (hbutton->is_debouncing)
			hbutton->is_debouncing = false;

		return;
	}

	original_st = hbutton->state;
	hbutton->state = BUTTON_debouncing(hbutton, new_st);

	BUTTON_mark_pressed_time(hbutton);
	hbutton->is_clicked = BUTTON_mark_is_click(original_st, hbutton->state);
}
#endif

bool BUTTON_is_click(pButton_TypeDef hbutton)
{
	return hbutton->is_clicked;
}

void BUTTON_clear_is_click(pButton_TypeDef hbutton)
{
	hbutton->is_clicked = false;
}

bool BUTTON_is_double_pressed(pButton_TypeDef hbutton, uint32_t interval)
{
	uint32_t tmp;

	if (hbutton->pressed_cnt < 2)
		return false;

	tmp = hbutton->pressed_tick[1] - hbutton->pressed_tick[0];
	if (tmp > interval)
		return false;

	if (HAL_GetTick() - hbutton->pressed_tick[1] >= interval)
		return false;

	return true;
}

void BUTTON_clear_timestamp(pButton_TypeDef hbutton)
{
	hbutton->pressed_cnt = 0;
	hbutton->pressed_tick[0] = 0;
	hbutton->pressed_tick[1] = 0;
}

bool USB_connected(void)
{
	return (HAL_GPIO_ReadPin(USB_DET_PORT, USB_DET_PIN) == USB_DET_CONNECT_ST);
}
