/*
 * charger.c
 *
 *  Created on: Aug 5, 2022
 *      Author: issac
 */

/* Includes ----------------------------------------------------------------- */
#include "charger.h"
#include "hw_config.h"


/* Private typedef ---------------------------------------------------------- */


/* Private variables -------------------------------------------------------- */


/* Public variables --------------------------------------------------------- */


/* Private function prototypes ---------------------------------------------- */


/* Private function --------------------------------------------------------- */


/* Public function ---------------------------------------------------------- */
void CHARGER_set(CHARGER_ST charger_st)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	if (charger_st == CHARGER_ST_ENABLE)
	{
		HAL_GPIO_DeInit(CHGR_CTRL_PORT, CHGR_CTRL_PIN);
	}
	else
	{
		/* Configure GPIO pin Output Level */
		HAL_GPIO_WritePin(CHGR_CTRL_PORT, CHGR_CTRL_PIN, CHGR_CTRL_DIS_ST);

		/* Configure GPIO pin : PA12 */
		GPIO_InitStruct.Pin = CHGR_CTRL_PIN;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(CHGR_CTRL_PORT, &GPIO_InitStruct);
	}
}
